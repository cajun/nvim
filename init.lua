---@module NeoVim configuration
---@author Zac Kleinpeter
---@license MIT

-- Clone 'mini.nvim' manually in a way that it gets managed by 'mini.deps'
local path_package = vim.fn.stdpath("data") .. "/site/"
local mini_path = path_package .. "pack/deps/start/mini.nvim"
if not vim.loop.fs_stat(mini_path) then
	vim.cmd('echo "Installing `mini.nvim`" | redraw')
	local clone_cmd = {
		"git",
		"clone",
		"--filter=blob:none",
		"https://github.com/echasnovski/mini.nvim",
		mini_path,
	}
	vim.fn.system(clone_cmd)
	vim.cmd("packadd mini.nvim | helptags ALL")
	vim.cmd('echo "Installed `mini.nvim`" | redraw')
end

-- Set up 'mini.deps' (customize to your liking)
require("mini.deps").setup()

local add, now, later = MiniDeps.add, MiniDeps.now, MiniDeps.later

-- Color scheme setups
now(function()
	-- add("fynnfluegge/monet.nvim")
	-- add("AlexvZyl/nordic.nvim")
	-- add("sainnhe/everforest")
	-- add("f4z3r/gruvbox-material.nvim")
	-- add("zenbones-theme/zenbones.nvim")
	-- add("folke/tokyonight.nvim")
	-- add("rebelot/kanagawa.nvim")
	-- add("bluz71/vim-moonfly-colors")
	-- add("bluz71/vim-nightfly-colors")
	-- add("savq/melange-nvim")
	-- add("aktersnurra/no-clown-fiesta.nvim")
	--
	-- add("aliqyan-21/darkvoid.nvim")
	-- require("darkvoid").setup({
	-- 	transparent = true,
	-- 	glow = true,
	-- })
	--
	-- add("WTFox/jellybeans.nvim")
	add("nuvic/flexoki-nvim")
	require("flexoki").setup({ variant = "moon" })
end)

-- Safely execute immediately
-- General setup of default settings
now(function()
	vim.g.mapleader = " "
	vim.g.maplocalleader = " "

	vim.o.relativenumber = true
	vim.o.undodir = vim.fn.stdpath("config") .. "/undo"
	vim.o.backupdir = vim.fn.stdpath("config") .. "/backup"
	vim.o.directory = vim.fn.stdpath("config") .. "/swap"
	vim.o.undolevels = 1000
	vim.o.undoreload = 10000
	vim.o.backupcopy = "yes"
	vim.o.tabstop = 2
	vim.o.expandtab = true
	vim.o.shiftwidth = 2
	vim.o.scrolloff = 10
	vim.o.formatprg = "par  -w80"
	vim.o.termguicolors = true

	vim.o.background = "dark"
	-- vim.cmd("colorscheme gruvbox-material")
	-- vim.cmd("colorscheme tokyonight-storm")
	-- vim.cmd("colorscheme everforest")
	-- vim.cmd("colorscheme kanagawa")
	-- vim.cmd("colorscheme moonfly")
	-- vim.cmd("colorscheme nightfly")
	-- vim.cmd("colorscheme melange")
	-- vim.cmd("colorscheme randomhue")
	-- vim.cmd("colorscheme no-clown-fiesta")
	-- vim.cmd("colorscheme minicyan")
	-- vim.cmd("colorscheme darkvoid")
	-- vim.cmd("colorscheme jellybeans")
	vim.cmd("colorscheme flexoki")
	-- vim.cmd("colorscheme minicyan")
	-- vim.cmd("colorscheme darkvoid")
end)

-- Setting up mini plugins
later(function()
	-- require("mini.colors").setup()
	-- require("mini.hues").setup()
	-- require("mini.base16").setup()
	-- require("mini.comment").setup()
	-- require("mini.bufremove").setup()
	-- require("mini.jump").setup()
	-- require("mini.jump2d").setup()
	-- require("mini.move").setup()

	require("mini.ai").setup()
	require("mini.align").setup()
	require("mini.basics").setup({ mappings = { option_toggle_prefix = "t" } })
	require("mini.bracketed").setup()
	require("mini.completion").setup()
	require("mini.cursorword").setup()
	require("mini.diff").setup({
		view = {
			style = "sign",
			signs = { add = "┃", change = "┃", delete = "_" },
		},
	})
	require("mini.doc").setup()
	require("mini.fuzzy").setup()
	require("mini.git").setup()
	require("mini.indentscope").setup({ symbol = "┃" })
	require("mini.icons").setup()
	require("mini.misc").setup()
	-- require("mini.notify").setup()
	require("mini.operators").setup()
	-- require("mini.pairs").setup()
	require("mini.sessions").setup()
	require("mini.splitjoin").setup()
	require("mini.statusline").setup({ use_icons = true })
	require("mini.surround").setup()
	require("mini.trailspace").setup()
	require("mini.visits").setup()
end)

-- MiniMap is rarely used, but is a very neat visual
later(function()
	local map = require("mini.map")
	local diagnostic_integration = map.gen_integration.diff({
		add = "MiniDiffSignAdd",
		change = "MiniDiffSignChange",
		delete = "MiniDiffSignDelete",
	})

	map.setup({ integrations = { diagnostic_integration } })

	local keymap = vim.keymap.set

	keymap("n", "<leader>mm", map.toggle, { desc = "Toggle Code Map" })
end)

-- MiniFiles is a nice plugin.  My normal workflow is to use Oil instead
later(function()
	require("mini.files").setup()

	local minifiles_toggle = function(...)
		if not MiniFiles.close() then
			MiniFiles.open()
		end
	end

	local keymap = vim.keymap.set

	keymap("n", "<leader>mf", minifiles_toggle, { desc = "Toggle Mini Files" })
end)

later(function()
  add("rafamadriz/friendly-snippets")

	local gen_loader = require("mini.snippets").gen_loader
	require("mini.snippets").setup({
		snippets = {
			-- Load custom file with global snippets first (adjust for Windows)
			gen_loader.from_file("~/.config/nvim/snippets/global.json"),

			-- Load snippets based on current language by reading files from
			-- "snippets/" subdirectories from 'runtimepath' directories.
			gen_loader.from_lang(),
		},
	})
end)
-- A nice LSP status notification
now(function()
	add("j-hui/fidget.nvim")
	require("fidget").setup()
end)

-- Extra settings for notifiy
-- and autocomplete settings
later(function()
	-- Add <Tab> on auto complete to change option
	vim.api.nvim_set_keymap("i", "<Tab>", [[pumvisible() ? "\<C-n>" : "\<Tab>"]], { noremap = true, expr = true })
	vim.api.nvim_set_keymap("i", "<S-Tab>", [[pumvisible() ? "\<C-p>" : "\<S-Tab>"]], { noremap = true, expr = true })

	-- Remove indent scope for help screen
	vim.api.nvim_create_autocmd("FileType", {
		pattern = { "help", "alpha" },
		callback = function()
			vim.b.miniindentscope_disable = true
		end,
	})
end)

-- Adding icons Maybe not need them.   MiniIcons could be all that is needed
-- later(function()
-- 	add("nvim-tree/nvim-web-devicons")
-- end)

-- Soo awesome!!! easily access the file system
now(function()
	add("stevearc/oil.nvim")
	require("oil").setup({
		view_options = {
			show_hidden = true,
		},
	})
	vim.g.loaded_netrw = 1
	vim.g.loaded_netrwPlugin = 1
	vim.keymap.set("n", "-", "<cmd>:Oil<CR>", { desc = "Open Directory" })
end)

-- Lsp Format on Save
now(function()
	add("lukas-reineke/lsp-format.nvim")
end)

-- Manually config the lsp serevers
-- See https://github.com/neovim/nvim-lspconfig for more information
-- NOTE: you will need the lsp server installed to use the lspconfig
now(function()
	add({
		source = "neovim/nvim-lspconfig",
		depends = {
			"lukas-reineke/lsp-format.nvim",
		},
	})

	require("lsp-format").setup({})

	local on_attach = function(client, bufnr)
		require("lsp-format").on_attach(client, bufnr)
	end

	require("lspconfig").ruby_lsp.setup({ on_attach = on_attach }) -- ruby
	require("lspconfig").tailwindcss.setup({ on_attach = on_attach }) -- Tailwindcss
	require("lspconfig").ts_ls.setup({ on_attach = on_attach }) -- Javascript / Typescript
end)

-- Plugin to assists with keyboard commands
later(function()
	local miniclue = require("mini.clue")
	miniclue.setup({
		triggers = {
			-- Leader triggers
			{ mode = "n", keys = "<Leader>" },
			{ mode = "x", keys = "<Leader>" },

			-- Built-in completion
			{ mode = "i", keys = "<C-x>" },

			-- mini.basic
			{ mode = "n", keys = [[\]] },
			-- mini.bracketed
			{ mode = "n", keys = "[" },
			{ mode = "n", keys = "]" },
			{ mode = "x", keys = "[" },
			{ mode = "x", keys = "]" },

			-- `g` key
			{ mode = "n", keys = "g" },
			{ mode = "x", keys = "g" },

			-- Marks
			{ mode = "n", keys = "'" },
			{ mode = "n", keys = "`" },
			{ mode = "x", keys = "'" },
			{ mode = "x", keys = "`" },

			-- Registers
			{ mode = "n", keys = '"' },
			{ mode = "x", keys = '"' },
			{ mode = "i", keys = "<C-r>" },
			{ mode = "c", keys = "<C-r>" },

			-- Window commands
			{ mode = "n", keys = "<C-w>" },

			-- `z` key
			{ mode = "n", keys = "z" },
			{ mode = "x", keys = "z" },

			{ mode = "n", keys = "t" },
			{ mode = "x", keys = "t" },
		},
		window = {
			delay = 500,
		},
		clues = {
			{ mode = "n", keys = "<Leader>l", desc = "+LSP" },
			{ mode = "n", keys = "<Leader>s", desc = "+Search" },
			{ mode = "n", keys = "<Leader>t", desc = "+Test" },
			{ mode = "n", keys = "<Leader>m", desc = "+Mini" },
			{ mode = "n", keys = "<Leader>d", desc = "+Dadbod" },
			-- Enhance this by adding descriptions for <Leader> mapping groups
			miniclue.gen_clues.builtin_completion(),
			miniclue.gen_clues.g(),
			miniclue.gen_clues.marks(),
			miniclue.gen_clues.registers(),
			miniclue.gen_clues.windows(),
			miniclue.gen_clues.z(),
		},
	})
end)

-- Custom hilighting patterns
later(function()
	require("mini.extra").setup()

	local hi_words = MiniExtra.gen_highlighter.words
	require("mini.hipatterns").setup({
		highlighters = {
			-- Highlight standalone 'FIXME', 'HACK', 'TODO', 'NOTE'
			fixme = hi_words({ "FIXME", "Fixme", "fixme" }, "MiniHipatternsFixme"),
			hack = hi_words({ "HACK", "Hack", "hack" }, "MiniHipatternsHack"),
			todo = hi_words({ "TODO", "Todo", "todo" }, "MiniHipatternsTodo"),
			note = hi_words({ "NOTE", "Note", "note" }, "MiniHipatternsNote"),

			-- Highlight hex color strings (`#rrggbb`) using that color
			hex_color = require("mini.hipatterns").gen_highlighter.hex_color(),
		},
	})
end)

-- Autocomplete
later(function()
	-- Centered on screen
	local win_config = function()
		local height = math.floor(0.618 * vim.o.lines)
		local width = math.floor(0.618 * vim.o.columns)
		return {
			anchor = "NW",
			height = height,
			width = width,
			row = math.floor(0.5 * (vim.o.lines - height)),
			col = math.floor(0.5 * (vim.o.columns - width)),
		}
	end

	require("mini.pick").setup({ window = { config = win_config }, options = { content_from_bottom = true } })
end)

-- Formatters and Linters
-- NOTE: the programs will need to be installed to work
now(function()
	add({ source = "creativenull/efmls-configs-nvim", depends = { "neovim/nvim-lspconfig" } })

	local buffmt = require("efmls-configs.formatters.buf")
	local buflint = require("efmls-configs.linters.buf")
	local jq = require("efmls-configs.linters.jq")
	local djlint = require("efmls-configs.linters.djlint")
	local dprint = require("efmls-configs.formatters.dprint")
	local eslint_d = require("efmls-configs.linters.eslint_d")
	local hadolint = require("efmls-configs.linters.hadolint")
	local prettier = require("efmls-configs.formatters.prettier")
	local prettier_d = require("efmls-configs.formatters.prettier_d")
	local rustfmt = require("efmls-configs.formatters.rustfmt")
	local shellcheck = require("efmls-configs.linters.shellcheck")
	local shfmt = require("efmls-configs.formatters.shfmt")
	local sql_formatter = require("efmls-configs.formatters.sql-formatter")
	local sqlfluff = require("efmls-configs.linters.sqlfluff")
	local stylelint = require("efmls-configs.linters.stylelint")
	local stylua = require("efmls-configs.formatters.stylua")
	local yamllint = require("efmls-configs.linters.yamllint")

	local languages = {
		-- NOTE: using standard for javascript formatting and linting
		javascript = { eslint_d, prettier_d },
		typescript = { eslint_d, prettier_d },
		typescriptreact = { eslint_d, prettier_d },
		json = { jq, prettier_d },
		lua = { stylua },
		sh = { shellcheck, shfmt },
		css = { prettier_d, stylelint },
		docker = { hadolint },
		html = { djlint, prettier_d },
		proto = { buflint, buffmt },
		eruby = {
			{
				formatCommand = "htmlbeautifier --stop-on-errors --keep-blank-lines 2",
				formatStdin = true,
			},
		},
		rust = { rustfmt },
		sql = { sqlfluff, sql_formatter },
		toml = { dprint },
		yaml = { yamllint, prettier },
		markdown = { dprint },
		roslyn = { dprint },
	}

	local on_attach = function(client, bufnr)
		require("lsp-format").on_attach(client, bufnr)
	end

	local efmls_config = {
		filetypes = vim.tbl_keys(languages),
		settings = {
			rootMarkers = { ".git/" },
			languages = languages,
		},
		init_options = {
			documentFormatting = true,
			documentRangeFormatting = true,
		},
		on_attach = on_attach,
	}

	require("lspconfig").efm.setup(vim.tbl_extend("force", efmls_config, {}))
end)

-- Rust LSP
now(function()
	add({
		source = "mrcjkb/rustaceanvim",
		checkout = "v5.19.2",
		monitor = "master",
	})

	vim.g.rustaceanvim = {
		server = {
			default_settings = {
				["rust-analyzer"] = {
					cargo = { loadOutDirsFromCheck = true },
					checkOnSave = { command = "clippy" },
					procMacro = { enable = true },
				},
			},
		},
	}
end)

-- Adding diagnostic icons
-- Sorting diagnostic by error level
now(function()
	vim.diagnostic.config({
		signs = {
			text = {
				[vim.diagnostic.severity.ERROR] = "",
				[vim.diagnostic.severity.HINT] = "",
				[vim.diagnostic.severity.INFO] = "",
				[vim.diagnostic.severity.WARN] = "",
			},
		},
		severity_sort = true,
	})
end)

-- The legend tpope
--
now(function()
	add("tpope/vim-rails")
	add("tpope/vim-fugitive")
	add("tpope/vim-dispatch")
	-- add("tpope/vim-commentary")
	add("tpope/vim-dadbod")
	add("kristijanhusak/vim-dadbod-completion")
	add("kristijanhusak/vim-dadbod-ui")
	vim.g.db_ui_use_nerd_fonts = 1

	local nmap_leader = function(suffix, rhs, desc, opts)
		opts = opts or {}
		opts.desc = desc
		vim.keymap.set("n", "<Leader>" .. suffix, rhs, opts)
	end

	nmap_leader("dt", "<Cmd>DBUIToggle<CR>", "Dadbod Toggle")
end)

-- Treesitter for all the things
now(function()
	add({
		source = "nvim-treesitter/nvim-treesitter",
		-- Use 'master' while monitoring updates in 'main'
		checkout = "master",
		monitor = "main",
		-- Perform action after every checkout
		hooks = {
			post_checkout = function()
				vim.cmd("TSUpdate")
			end,
		},
	})

	add("nvim-treesitter/playground")

	require("nvim-treesitter.configs").setup({
		ensure_installed = "all",
		highlight = { enable = true },
		-- ruby has an issue with indention
		indent = { enable = true, disable = { "ruby" } },
		incremental_selection = { enable = true },
		query_linter = { enable = true },
		context_commentstring = { enable = true },
		playground = { enable = true },
	})

	vim.o.foldmethod = "expr"
	vim.o.foldexpr = "nvim_treesitter#foldexpr()"
	vim.o.foldenable = false
end)

-- Keyboard shortcuts for pickers
later(function()
	local keymap = vim.keymap.set
	local MiniPick = require("mini.pick")
	local MiniExtra = require("mini.extra")

	keymap("n", "<leader><space>", MiniPick.builtin.buffers, { desc = "Find Buffers" })
	keymap("n", "<leader>sf", MiniPick.builtin.files, { desc = "Find Files" })
	keymap("n", "<leader>sh", MiniPick.builtin.help, { desc = "Find Helptags" })
	keymap("n", "<leader>sd", MiniExtra.pickers.diagnostic, { desc = "Find Diagnostic" })
	keymap("n", "<leader>ss", MiniExtra.pickers.history, { desc = "Find History" })
	keymap("n", "<leader>sb", MiniExtra.pickers.buf_lines, { desc = "Find Lines" })
	keymap("n", "<leader>st", MiniExtra.pickers.treesitter, { desc = "Find TS" })
	keymap("n", "<leader>sk", MiniExtra.pickers.keymaps, { desc = "Find Keys" })
	keymap("n", "<leader>sc", MiniExtra.pickers.commands, { desc = "Find Commands" })
	keymap("n", "<leader>sp", MiniPick.builtin.grep_live, { desc = "Find Live Grep" })
	keymap("n", "<leader>sl", function()
		return MiniExtra.pickers.lsp({ scope = "document_symbol" })
	end, { desc = "Find LSP Symbols" })
	keymap("n", "<leader>sr", function()
		return MiniExtra.pickers.lsp({ scope = "references" })
	end, { desc = "Find LSP Refs" })
end)

-- keyboard shortcuts for lsp
later(function()
	local nmap_leader = function(suffix, rhs, desc, opts)
		opts = opts or {}
		opts.desc = desc
		vim.keymap.set("n", "<Leader>" .. suffix, rhs, opts)
	end

	nmap_leader("la", "<Cmd>lua vim.lsp.buf.code_action()<CR>", "Code Action")
	nmap_leader("ls", "<Cmd>lua vim.lsp.buf.signature_help()<CR>", "Arguments popup")
	nmap_leader("li", "<Cmd>lua vim.lsp.buf.hover()<CR>", "Information")
	nmap_leader("lj", "<Cmd>lua vim.diagnostic.goto_next()<CR>", "Next diagnostic")
	nmap_leader("lk", "<Cmd>lua vim.diagnostic.goto_prev()<CR>", "Prev diagnostic")
	nmap_leader("lR", "<Cmd>lua vim.lsp.buf.references()<CR>", "References")
	nmap_leader("lr", "<Cmd>lua vim.lsp.buf.rename()<CR>", "Rename")
	nmap_leader("ld", "<Cmd>lua vim.lsp.buf.definition()<CR>", "Source definition")
end)

-- Expand snippet
-- This is useful in rust code
-- now(function()
-- 	-- You can add this in your init.lua
-- 	-- or a plugin script
--
-- 	local function expand_snippet(event)
-- 		local comp = vim.v.completed_item
-- 		local kind = vim.lsp.protocol.CompletionItemKind
-- 		local item = vim.tbl_get(comp, "user_data", "nvim", "lsp", "completion_item")
--
-- 		-- Check that we were given a snippet
-- 		if
-- 			not item
-- 			or not item.insertTextFormat
-- 			or not item.textEdit
-- 			or not item.textEdit.newText
-- 			or item.insertTextFormat == 1
-- 			-- or not (item.kind == kind.Snippet or item.kind == kind.Keyword)
-- 		then
-- 			return
-- 		end
--
-- 		-- Remove the inserted text
-- 		local cursor = vim.api.nvim_win_get_cursor(0)
-- 		local line = vim.api.nvim_get_current_line()
-- 		local lnum = cursor[1] - 1
-- 		local start_col = cursor[2] - #comp.word
--
-- 		if start_col < 0 then
-- 			return
-- 		end
--
-- 		local set_text = vim.api.nvim_buf_set_text
-- 		-- set_text(event.buf, lnum, start_col, lnum, #line, { "" })
-- 		local ok = pcall(set_text, event.buf, lnum, start_col, lnum, #line, { "" })
--
-- 		if not ok then
-- 			return
-- 		end
--
-- 		-- Insert snippet
-- 		local snip_text = vim.tbl_get(item, "textEdit", "newText") or item.insertText
--
-- 		assert(snip_text, "Language server indicated it had a snippet, but no snippet text could be found!")
--
-- 		-- warning: this api is not stable yet
-- 		vim.snippet.expand(snip_text)
-- 	end
--
-- 	vim.api.nvim_create_autocmd("CompleteDone", {
-- 		desc = "Expand LSP snippet",
-- 		callback = expand_snippet,
-- 	})
-- end)

-- Screen saver == just fun
later(function()
	add("NStefan002/donut.nvim")
end)

now(function()
	add("danymat/neogen")
	require("neogen").setup({})
end)

later(function()
	add("nvchad/showkeys")
	require("showkeys")
end)

now(function()
	add("garymjr/nvim-snippets")
end)

now(function()
	vim.api.nvim_create_autocmd({ "BufNewFile", "BufRead" }, {
		pattern = { "*.yml" },
		callback = function()
			vim.o.syntax = "yaml"
		end,
	})
end)

-- now(function()
-- 	add({
--
-- 		source = "glacambre/firenvim",
-- 		hooks = {
-- 			post_checkout = function()
-- 				vim.cmd("firenvim#install(0)")
-- 			end,
-- 		},
-- 	})
-- end)

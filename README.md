# The vim Config

## NeoVim Plugins

- efm-langserver.lua **general purpose language server**
- gitsigns.lua **git in gutter**
- lsp_config.lua **lsp keybindings**
- mini.lua **collection of awesome plugins**
- oil.lua **file exploration in a buffer** 
- tpope.lua **an amazing plugin creator**
- treesitter.lua **treesitter config**
